//This is my MiniX1 - the first exercise in p5.js
//By Mathilde Borregaard Gajhede

//For this code I've found inspiration in watercoloring, and I wanted to make
//it look like palette with colors and outside you're able to "paint" yourself
function setup() {
//I made the Canvas 1438 by 790 after a lot of trial and error I found this
// looked the best
  createCanvas(1438,790);
  print("hello world");

  //Put the background in setup, so I can paint with my cursor later on
    background('#89BCCF');
}

function draw() {

//For the colors including the background I've used HTML color codes found on
//https://htmlcolorcodes.com

//The white background on the palette with an outline
let k = color('#FFFFFF');
fill(k);
strokeWeight(1);
stroke(51);
rectMode(CENTER)
rect(719, 395, 695, 170);

//Color 1 = GRASS
let a = color(100, 204, 0);
fill(a);
noStroke();
rectMode(CORNER)
rect(382, 330, 45, 80,5);

textSize(9);
fill(50);
text('GRASS', 388, 430);

//Color 2 = LIME
let b = color('#E0ED41');
fill(b);
noStroke();
rect(452, 330, 45, 80,5);

fill(50);
text('LIME', 462, 430);

//Color 3 = SUNSET

let c = color(300, 204, 0);
fill(c);
noStroke();
rect(522, 330, 45, 80,5);

fill(50);
text('SUNSET', 526, 430);

//Color 4 = FIRE

let d = color('#F8981D');
fill(d);
noStroke();
rect(592, 330, 45, 80,5);

fill(50);
text('FIRE', 604, 430);

//Color 5 = ALARM

let e = color('#DF4B4B');
fill(e);
noStroke();
rect(662, 330, 45, 80,5);

fill(50);
text('ALARM', 667, 430);

//Color 6 = BABYDOLL

let f = color('#DC83A5');
fill(f);
noStroke();
rect(732, 330, 45, 80,5);

fill(50);
text('BABYDOLL', 733, 430);

//Color 7 = VIOLET

let g = color('#9E89CF');
fill(g);
noStroke();
rect(802, 330, 45, 80,5);

fill(50);
text('VIOLET', 809, 430);

//Color 8 = DOVE

let h = color('#89BCCF');
fill(h);
noStroke();
rect(872, 330, 45, 80,5);

fill(50);
text('DOVE', 881, 430);

//Color 9 = MAGIC

let i = color('#5055C0');
fill(i);
noStroke();
rect(942, 330, 45, 80,5);

fill(50);
text('MAGIC', 948, 430);

//Color 10 = MARINE

let j = color('#282E72');
fill(j);
noStroke();
rect(1012, 330, 45, 80,5);

fill(50);
text('MARINE', 1017, 430);

//The "paintbrush" - an ellipse with less opacity (added alpha)

noStroke()
fill(250,200,200,50)
ellipse(mouseX,mouseY,7,7);




}
