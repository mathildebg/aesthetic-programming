//1st rule - for every ellipse draw a new one 40 pixels to the right, when it
//reaches the width of the screen continue 40 pixels below

//2nd rule - the further down the x-axis the wider the ellipses
//and the further down the y-axis the longer the ellipses


let x=0;
let y=0;
let spacing=40;

var col = {
r:255,
g:255,
b:255
}

function setup(){
createCanvas(windowWidth,windowHeight);
background(223,190,241);
}

function draw(){

  col.r = random(180,255);
  col.g = random(180,255);
  col.b = random(180,255);

  fill(col.r,col.g,col.b);
  noStroke()

//random function will choose a number between 0 and 1 with random(1)
//we're putting that information into a conditional statement

if(random(1)<0.5){
  //x and y are the placement of the ellipses (underneath defined as movin 40
  //pixels each time)
  //for the size here x would be zero and y would be 40 at position (0,0), but
  //both parameters will get bigger as is moves across the page
ellipse(x,y,x,y+spacing);
}
else {
  //for the size here y would be zero and x would be 40 at position (0,0)
ellipse(x,y,x+spacing,y);
}

//to make the movement on the page

    x=x+spacing
//and a while loop to let the drawing of the ellipses move to the next page

    while(x>width){
x=0;
y=y+spacing
}


  }
