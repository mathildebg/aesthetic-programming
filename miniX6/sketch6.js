var c = "black";

function setup() {
  createCanvas(windowWidth,windowHeight);
	background(200,229,227);
}

function draw() {

  if (mouseIsPressed) {
		stroke(c);
    line(mouseX, mouseY, pmouseX, pmouseY);
  }
  //The white background on the palette with an outline

  fill(color(255,255,255));
  strokeWeight(1);
  stroke(51);

  rect(372, 320, 695, 170);

	noStroke();
  //draw the GRASS
  fill(100, 204, 0);
  noStroke();
  rect(382, 330, 45, 80);

  textSize(9);
  fill(50);
  text('GRASS', 388, 430);

  //draw the LIME
  fill('#E0ED41');
  noStroke();
  rect(452, 330, 45, 80);

  fill(50);
  text('LIME', 462, 430);

  //draw the SUNSET
  fill(300, 204, 0);
  noStroke();
  rect(522, 330, 45, 80);

  fill(50);
  text('SUNSET', 526, 430);

  //draw the FIRE
  fill('#F8981D');
  noStroke();
  rect(592, 330, 45, 80,5);

  fill(50);
  text('FIRE', 604, 430);

  //draw the ALARM
  fill('#DF4B4B');
  noStroke();
  rect(662, 330, 45, 80,5);

  fill(50);
  text('ALARM', 667, 430);

  // draw the BABYDOLL
  fill('#DC83A5');
  noStroke();
  rect(732, 330, 45, 80,5);

  fill(50);
  text('BABYDOLL', 733, 430);

  //draw the VIOLET
  fill('#9E89CF');
  noStroke();
  rect(802, 330, 45, 80,5);

  fill(50);
  text('VIOLET', 809, 430);


  //draw the DOVE
  fill('#89BCCF');
  noStroke();
  rect(872, 330, 45, 80,5);

  fill(50);
  text('DOVE', 881, 430);


  //draw the MAGIC
  fill('#5055C0');
  noStroke();
  rect(942, 330, 45, 80,5);

  fill(50);
  text('MAGIC', 948, 430);


  //draw the MARINE
  fill('#282E72');
  noStroke();
  rect(1012, 330, 45, 80,5);

  fill(50);
  text('MARINE', 1017, 430);

}

//this will run whenever the mouse is pressed
function mousePressed() {
  if (mouseX > 382 && mouseX < 427 && mouseY > 330 && mouseY < 410) {
    //set the variables to random values
    c = color(100, 204, 0)
  }

  if (mouseX > 452 && mouseX < 497 && mouseY > 330 && mouseY < 410 ) {
    //set the variables to random values
    c = '#E0ED41';
  }

  if (mouseX > 522 && mouseX < 567 && mouseY > 330 && mouseY < 410 ) {
    //set the variables to random values
    c = color(300, 204, 0);
  }

  if (mouseX > 592 && mouseX < 637 && mouseY > 330 && mouseY < 410 ) {
    //set the variables to random values
    c = '#F8981D';
  }

  if (mouseX > 662 && mouseX < 707 && mouseY > 330 && mouseY < 410 ) {
    //set the variables to random values
    c = '#DF4B4B';
  }

  if (mouseX > 732 && mouseX < 777 && mouseY > 330 && mouseY < 410 ) {
    //set the variables to random values
    c = '#DC83A5';
  }

  if (mouseX > 802 && mouseX < 847 && mouseY > 330 && mouseY < 410 ) {
    //set the variables to random values
    c = '#9E89CF';
  }

  if (mouseX > 872 && mouseX < 917 && mouseY > 330 && mouseY < 410 ) {
    //set the variables to random values
    c = '#89BCCF';
  }

  if (mouseX > 942 && mouseX < 987 && mouseY > 330 && mouseY < 410 ) {
    //set the variables to random values
    c = '#5055C0';
  }

  if (mouseX > 1012 && mouseX < 1057 && mouseY > 330 && mouseY < 410 ) {
    //set the variables to random values
    c = '#282E72';
  }
}
