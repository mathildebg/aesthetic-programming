
function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(20);
  background(229,201,201);
  print("hello world");
}

  function draw(){

//text is written here
  fill(50);
  textSize(50)
  textAlign(CENTER);
  text('HOW DO YOU ACTUALLY FEEL?', 0.5*windowWidth+7, 300);

  textSize(20)
  text('HOW DO  YOU ACTUALLY FEEL?',200,100)

  textSize(30)
  text('HOW DO  YOU ACTUALLY FEEL?',1200,200)

  textSize(10)
  text('HOW DO  YOU ACTUALLY FEEL?',900,250)

  textSize(25)
  text('HOW DO  YOU ACTUALLY FEEL?',350,250)

  textSize(15)
  text('HOW DO  YOU ACTUALLY FEEL?',800,40)

  textSize(5)
  text('HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL? HOW DO  YOU ACTUALLY FEEL?',1,windowHeight*0.9)

//text describing the emojis
textSize(10)
text('WHEN YOU PUSH MY BUTTONS',windowWidth*0.25,windowHeight*0.6)

textSize(10)
text('I AM INDIFFERENT',windowWidth*0.50,windowHeight*0.6)

textSize(10)
text('WHEN CODING CLICKED',windowWidth*0.75,windowHeight*0.6)

//THE THREE BIG EMOJIS

//1st big yellow ellipse
fill('yellow')
ellipse(windowWidth*0.25,windowHeight*0.5,100,100);
//mouth
fill('white')
arc(windowWidth*0.25, windowHeight*0.5+20, 70, 50, 3.14, 0, CHORD)
//white in the eyes
fill('white')
ellipse(windowWidth*0.25-20,windowHeight*0.5-20,30,30)
ellipse(windowWidth*0.25+20,windowHeight*0.5-20,30,30)
//black in the eyes
fill('black')
ellipse(windowWidth*0.25-20,windowHeight*0.5-20,10,10)
ellipse(windowWidth*0.25+20,windowHeight*0.5-20,10,10)

//2nd big yellow ellipse
fill('yellow')
ellipse(windowWidth*0.5,windowHeight*0.5,100,100);
//mouth
arc(windowWidth*0.50,windowHeight*0.5, 60, 40, 0, 2 + 0, 0.4*PI);
//white in the eyes
fill('white')
ellipse(windowWidth*0.50-20,windowHeight*0.5-20,30,30);
ellipse(windowWidth*0.50+20,windowHeight*0.5-20,30,30);
//black in the eyes
fill('black')
ellipse(windowWidth*0.50-20,windowHeight*0.5-20,10,10)
ellipse(windowWidth*0.50+20,windowHeight*0.5-20,10,10);

//3rd big yellow ellipse
fill('yellow')
ellipse(windowWidth*0.75,windowHeight*0.5,100,100);
//mouth
fill('white')
arc(windowWidth*0.75, windowHeight*0.5, 70, 50, 0, 3.14, CHORD);
//white in the eyes
fill('white')
ellipse(windowWidth*0.75-20,windowHeight*0.5-20,30,30)
ellipse(windowWidth*0.75+20,windowHeight*0.5-20,30,30)
//black in the eyes
fill('black')
ellipse(windowWidth*0.75-20,windowHeight*0.5-20,10,10)
ellipse(windowWidth*0.75+20,windowHeight*0.5-20,10,10)

//THREE EMOJIS FOR THE MOUSE TO DRAW

//1st - content smiley

fill('yellow')
ellipse(mouseX,mouseY,50,50);

fill('white')
ellipse(mouseX-10,mouseY-10,15,15);
ellipse(mouseX+10,mouseY-10,15,15);

fill('black')
ellipse(mouseX-10,mouseY-10,5,5)
ellipse(mouseX+10,mouseY-10,5,5)

fill('white')
arc(mouseX, mouseY, 30, 20, 0, 2 + 0, 0.4*PI);

//2nd - happy smiley

if(mouseIsPressed){

fill('yellow')
ellipse(mouseX,mouseY,50,50)

fill('white')
ellipse(mouseX-10,mouseY-10,15,15);
ellipse(mouseX+10,mouseY-10,15,15);

fill('black')
ellipse(mouseX-10,mouseY-10,5,5)
ellipse(mouseX+10,mouseY-10,5,5)

fill('white')
arc(mouseX, mouseY, 35, 25, 0, 3.14, CHORD);
}

//3rd - sad smiley
if(keyIsPressed){

 fill('yellow')
 ellipse(mouseX,mouseY,50,50)

 fill('white')
 ellipse(mouseX-10,mouseY-10,15,15);
 ellipse(mouseX+10,mouseY-10,15,15);


 fill('black')
 ellipse(mouseX-10,mouseY-10,5,5)
 ellipse(mouseX+10,mouseY-10,5,5)

 fill('white')
  arc(mouseX, mouseY+10, 35, 25, 3.14, 0, CHORD)
}

}
