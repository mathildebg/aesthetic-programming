# READ ME for my MiniX4
----
## MiniX4 for Aesthetic Programming at Aarhus University

![Screenchot of my first program](minix4nr1.png)
![Screenchot of my first program](minix4nr2.png)


### Link for my [MiniX4](https://mathildebg.gitlab.io/aesthetic-programming/miniX4/)
My RunMe is on the link above

### Link for my repository - [the code](https://gitlab.com/mathildebg/aesthetic-programming/-/blob/master/miniX4/sketch4.js)
My repository/ the code for the program is on the link above

----

### README

#### Title and short description of my work
My work is called "how is your 2021 going so far?" and is very simply a cat getting more and more frustruated and sad as the slider moves. We've all had to massively change our life in 2020, and I believe most people were looking forward to a fresh start in 2021, but it feels like it's just 2020 2.0 edition. That's why I've chosen a slider ranging from "meh" to "take me back to 2020" <br>
Not a lot of people have very positive notes on 2020 and 2021 is not looking better - so sometimes it's okay to just cry it out! It's all going to be okay < 333


#### Describe your program and what you've used and learnt
I think this weeks assignment pretty much reflects both how I've felt this week with my miniX4 but also how I'm starting to feel with 2021 in general. It actually felt like a great outlet, so that might be something I'd consider in the future as well. Actually my initial idea was to have a figure of some sort whose mouth got bigger and bigger with the input of the microphone, but for the life of me I couldn't make it work, which was super frustruating. <br>
So I sat down and started drawing a sad cat and worked on the idea from there. It's definitely not perfect and I wish I could've made the sound thing work, but it's okay - I'll manage in another assignment. <br>
So for this assignment I've used new syntax such as preload, slider and if/else statements, which was all very nice to get sorted out, since I haven't had the chance to until now. I find I'm learning a lot from just playing around with it, and I very easily get frustruated when I actually have to read or watch a video to make it work.. that might also be why it's taking me so long, but it's getting under my skin in a different way from copying, so I guess that's a good thing.. <br>
I got a bit of inspiration from instagram @herligesvend, who post animal memes portraying our current situation, which I've loved following during corona - it's been super helpful to have something to laugh about.  

#### How does this theme address the theme of "capture all"? What are the cultural implications of data capture?
I thought a lot about the quote from Jonathan Crary's book 24/7 which is presented in our Aesthetic Programming book as well: _“there is a relentless incursion of the non-time of 24/7 into every aspect of social or personal life. There are, for example, almost no circumstances now that cannot be recorded or archived as digital imagery or information.”_ <br>
I think this relates very well to the fact that most schools, uni, work and so on expect us to go on like nothing happened. I know this is not 100 percent true, and we generally tend to focus more on mental health at the moment, but our exams/deadlines/and so on haven't changed even though our whole life and perspective have. <br>
In society we do feel that numbers and data is a good way to classify people, and I really appreciate logic and maths, but to a certain extend. Capturing data on how fast we run, how symmetrical our faces are, how good our grades are on tests and so on - it only tells part of the story of a human life. Data needs to correlate with real life, and sometimes it's not even worth mentioning. <br>
Though my programme doesn't necessarily in itself capture data from the user, it is to be seen more as a statement. Right now with all these aspects in our lives expecting things from us and ideals we need to live up to - the data is less significant and the people are the most important always. AND it's okay to cry and feel frustruated even if you still have your school, have a job, have friends and everything else is looking good from the outside. We're all just human and life is a bit overwhelming at this time :))) 






