# READ ME for my MiniX6
----
## MiniX6 for Aesthetic Programming at Aarhus University

![Screenchot of my first program](Skærmbillede_2021-03-19_kl._23.31.19.png)



### Link for my [MiniX6](https://mathildebg.gitlab.io/aesthetic-programming/miniX6/)
My RunMe is on the link above

### Link for my repository - [the code](https://gitlab.com/mathildebg/aesthetic-programming/-/blob/master/miniX6/sketch6.js)
My repository/ the code for the program is on the link above

----

### README

#### Which MiniX do you plan to rework?
I planned to rework my very first miniX - not because I've improved my skills enough to make a significant difference, but because I had an ambition back then to make it more interactive, so I thought this was the way to go. 

#### What have you changed and why?
My very first miniX was super simple and I investigated in basic syntax such as rectangels and spent a lot of time on finding the right colors. Initially I wanted it to simulate a aquarelle palette with watercolors, because this is usually my creative outlet, so it was a way to combine the physical element of creativity to the programming language. But in my miniX1 I said following in the README:

_"If I were to continue working on this program I would examine ways of clicking the colors of the palette and thereby changing the color of the paintbrush around the mouse. However for this assignment I found it to be too difficult for my level of experience."_

I though this was a manageable task for me to do now, and though I did spend quite a while figuring out the best way to go about it - this was what I did for the miniX6. Now when you press a color and hold the mouse down you're able to draw with said color. That way you can almost use it as a drawing application. For my screenshot I just painted a bunch of faces on the page.

#### How would you demonstrate aesthetic programming in your work?
I guess that though most parts of aesthetics is relatively objective - the eye of humans is often pleased by the same things. For me what I love about being creative is when something is beautiful. I don't know that I'd say that my programme is beautiful in any way, but I've given the use a chance to create something themselves - hopefully something aesthetically pleasing. I really like this way of having an interactive piece to work with even though it's super simple. 

I feel like I'm lying pretty flat on the floor with this miniX, and I wanted to do better both conceptually and programming-wise, but my brain was pretty empty for ideas this time. Low-energy lewels and stressful week led to this way of demonstating aesthetic programming. 

#### What does it mean by programming as a practice, or even as a method for design? What is the relation between programming and digital culture?
I think programming can be seen as the most powerful tool for implementing design in the digital world. Basically without most people knowing, everything is controlled and monitored by some shape of programming - it holds the power in the digital world and thereby is a huge stake holder for the digital culture. For the programmer the difference between practice and method is highly linked with the concept of creative freedom.


#### Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?
I'm not too sure that my work demonstrates a certain perspective of critical-aesthetics seeing as this wasn't really my focus this time. I hope that I'll be bettering my conceptual focus, and hopefully answer this more fulfilling next time










