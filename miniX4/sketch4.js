let slider;
let kat1,kat2,kat3,kat4,kat5,kat6,kat7,kat8;

// pre-load function to avoid lag
function preload() {
  kat1 = loadImage('kat1.PNG');
  kat2 = loadImage('kat2.PNG');
  kat3 = loadImage('kat3.PNG');
  kat4 = loadImage('kat4.PNG');
  kat5 = loadImage('kat5.PNG');
  kat6 = loadImage('kat6.PNG');
  kat7 = loadImage('kat7.PNG');
  kat8 = loadImage('kat8.PNG');
  inconsolata = loadFont('Inconsolata-Black.otf');

}

function setup() {
  createCanvas(900, windowHeight);
  slider = createSlider(0, 7, 0);
  slider.position(50, 700);
  slider.size(800);



  }

  function draw() {
  let val = slider.value();

if(val==0){
  background(kat1)}

  else if(val==1){
  background(kat2)}

else if(val==2){
background(kat3)}

else if(val==3){
background(kat4)}

else if(val==4){
background(kat5)}

else if(val==5){
background(kat6)}

else if(val==6){
background(kat7)}

else if(val==7){
background(kat8)}

fill(85,166,187);
textFont(inconsolata)
textSize(20)
textAlign(CENTER);
text('how is your 2021 going so far?', width/2,100);
textSize(10)
text('blank', 50, 685);
text('meh', 175, 685);
text('aaargh', 280, 685);
text('i cry', 395, 685);
text('no me gusta', 510, 685);
text('cry me a river', 620, 685);
text('!!?!!?', 730, 685);
text('2020 bring me back', 850, 685);

}
