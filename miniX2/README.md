# READ ME for my MiniX2
----
## MiniX2 for Aesthetic Programming at Aarhus University


![Screenchot of my first program](miniX2.png)


### Link for my [MiniX2](https://mathildebg.gitlab.io/aesthetic-programming/miniX2/)
My RunMe is on the link above

### Link for my repository - [the code](https://gitlab.com/mathildebg/aesthetic-programming/-/blob/master/miniX2/sketch2.js)
My repository/the code for the program is on the link above

----

### README
#### Describe your program
My program consists of a pink background with three emojis centered on the page. Around the page is the text "How do you really feel?" in different sizes and placements. On the bottom of the page theres one long line with that sentence repeated. When you move the mouse around the page an emoji follows you around and draws the page with the yellow faces <br>
The first emoji is sad and underneath it says "When you push my buttons" which is obviously something that makes you annoyed or angry, but also it refers to the fact that when you press a button on the keyboard, the emoji following the mouse will change to the sad one. <br>
The second emoji is the indifferent one, and underneath it says "I am indifferent" - this is supposed to be the standard setting. How you "normally" look and also it is the emoji that as a standard follows the mouse around. <br>
The third emoji is the happy one and it's titled "When coding clicked" referring to the happy feeling you get when the code works out in your favour, but also when you click the mouse the emoji following you will change to the happy one. 


#### What have you used and learnt?
For this program I've focused a bit more in being confident with the geometrical references and the placement on the page. I've used the windowHeight and windowWidth a significant amount to place the emojis the right places. A new feature I used for this program was the arc() which I had quite a few issues with, but I worked out in the end. I also used the if(mouseIsPressed) and if(keyIsPressed) for the emojis following the mouse. <br>
It's mostly basic references I've used for this program, but I found it very helpful to get the basics under control for this week and play around with it before next week, where I believe it'll be a bit more complicated and specific. 

#### How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?
My program is called "How do you really feel?" and is supposed to look critically on the use of emojis in situations, where you don't actually use them as intended. <br>
The expression of the emoji depends on the context in which it is being used. That can differ from person to person but also depends on cultural differences. I understand the importance of representation in emojis, and I believe with emojis the cooperations set a certain standard and therefore they have the responsibilty to make the emojis reflect real life. <br>
That being said, it's a bit hard for me to relate to the problems, and I've chosed to set focus on something a bit less significant. I've often found that the use of emojis doesn't reflect how you actually feel in a certain situation. You could be super annoyed and still send a happy emoji and vise versa. Also some people have a certain language with the emojis and will be disappointed if they're not reciprocated - some people will even believe you're mad at them if you don't use emojis. <br>
I myself work in a customer service for an online store, and a lot of the brand is us being down-to-earth so it's pretty much embedded in the culture of the workspace that we use emojis, both for our customers, but also for my co-workers. We've had mostly positive response to this, but some people also find it extremely inappropriate. <br>
Therefore my programme is supposed to focus on what you actually mean - what are your intentions and how do you really feel? How is it received and how was it presented? What situations can change the use of emojis? Your relation to the receiver?




