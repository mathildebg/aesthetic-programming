# READ ME for my MiniX7
----
## MiniX7 for Aesthetic Programming at Aarhus University

![Screenchot of my first program](minix7.png)



### Link for my [MiniX7](https://mathildebg.gitlab.io/aesthetic-programming/miniX7/)
My RunMe is on the link above

### Link for my repository - [the code](https://gitlab.com/mathildebg/aesthetic-programming/-/blob/master/miniX7/sketch.js)
My repository/ the code for the program is on the link above

----

### README

#### How does/do your game/game objects work?
This easter I've sailed my new boat from Ebeltoft to Aalborg, and so I took inspiration in this trip. I've been a bit nervous about this trip since it's my first one were I was 100 percent responsible for the boat, and it's my own one. 
I've created a game were you act as my boat (Freja) and you have to jump to avoid hitting rocks on the bottom of the ocean. You press the space bar to jump and the rocks keep coming. 

####  How you program the objects and their related attributes, and the methods in your game.
I took a lot of inspiration in this video from Daniel Shiffman (https://www.youtube.com/watch?v=l0HoJHc-63Q&t=1368s) but I decided to draw my own background, rocks and the boat, which resembles my own. <br>
I have two classes; Sten and Baad. Both with their properties such as width and height, and behaviours as move() and show().  <br>
I really wanted to incooperate a way to count the amount of rocks as points with a distance function, but I couldn't make it work, so I gave up at last. 

#### Draw upon the assigned reading, are the characteristics of object-oriented programming and the wider implications of abstraction?
For OOP it is important to have focus on the properties and attributes of the objects in the programme. I must admit I probably wouldn't have been able to handle making this sort of programme without the help of Daniel Shiffman. Although when he decides on different properties for each object it does make sense, it wasn't something I'd initially think of. 

####  Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?
To be honest there wasn't much thought behind doing this game instead of others as I found it quite difficult to work this way. I was, however, extremely annoyed that I couldn't make counting points a function for the game, since I actually am beginning to question if it really is a game, if you can't win/loose it. 







