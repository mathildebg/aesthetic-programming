# READ ME for my MiniX1
----
## MiniX1 for Aesthetic Programming at Aarhus University

![Screenchot of my first program](mx1screenshot.png)


### Link for my [MiniX1](https://mathildebg.gitlab.io/aesthetic-programming/miniX1/)
My RunMe is on the link above

### Link for my repository - [the code](https://gitlab.com/mathildebg/aesthetic-programming/-/blob/master/miniX1/sketch.js)
My repository/ the code for the program is on the link above

----

### README
#### What have you produced?
For my program this first week of the course I've taken inspiration in a watercoloring palette. There's not really any deeper meaning to it, but I thought it would be a good way to play around with different colors and getting to know them more. I've decided to place the palette in the middle and added the blue color (I named it DOVE) to the background of the canvas. I wanted there to be something interactive about the program, so I added the "paintbrush" to the mouse-location. That way you can look at the watercoloring palette and paint around it - like I did in the screenshot with the flowers. <br>
If I were to continue working on this program I would examine ways of clicking the colors of the palette and thereby changing the color of the paintbrush around the mouse. However for this assignment I found it to be too difficult for my level of experience. 


#### How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?
A bit frustruating honestly. I had a bunch of different ideas and in a lot of cases I couldn't get them to work. I think for this assignment I was lucky that there was no particular task except getting familiar with the program, because then I had the option to settle with another idea. I imagine for the more specific programming tasks I'll get more frustruated, but at the same time - when it's finally a success it feels great! <br>
For this I must admit I didn't do a lot of thinking around the actual program other than wanting it to be pretty and fun, and therefore I also spent a good time choosing the different colors and finding HTML colorcodes. So I played a lot around with both colors and placing of the objects and at last with the paintbrush for the background.  <br>
I got the colors from https://htmlcolorcodes.com 

#### How is the coding process different from, or similar to, reading and writing text?
Well at this moment it feels very different, but I imagine as you get used to it, you'll not see it as very mcuh different. As it has been mentioned many times - it's like learning a new language and you just have to start out slow. I guess for this there's a certain way that is correct and the difference will then be that the code won't be accepted unless it's 100% correct whereas most people will still understand you if you make tiny errors when learning danish or so on. I guess you could say that people are more forgiving than the computer is - part of the fun, but also part of the frustration!

#### What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?
For me until now it has felt like coding was very inaccessible for me. I know if I wanted I could probably sit down and try to learn it, but it seems super complex from the outside and even now when I've learned a little bit I'm only realizing how little I actually know. I found myself having more respect for programmers at the very least. To be completely honest I don't know if I have had many reflections in the process that came from the assigned reading as I'm much more of a learning by doing person and everything seems overwhelming right now. But I definitely believe for future references I will use and reflect on both texts for last week. 







