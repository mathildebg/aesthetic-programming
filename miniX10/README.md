# READ ME for our MiniX9
----
## MiniX9 for Aesthetic Programming at Aarhus University

![Screenshot1](IdeaNumberOne.png)
#### _Idea number 1_
For our first idea we’re thinking of creating a sort of interactive game with the user. The user is presented with a picture and two collections of tags. One is generated and thereby biased by us (the human creators of the program) and one is generated from an API called EveryPixel (if we can make it work). The user then has to choose which tags carry the most resemblance with the photo on the site. The program is meant to put focus on both the bias we as humans have for certain pictures and how the machine will perceive the same picture. 


![Screenchot2](IdeaNumberTwo.png)
#### _Idea number 2_
For our second idea we wanted to focus more on generative poetry, but at the same time while working with the tags from the same API as the abovementioned idea. For each photo presented the tags that the API comes up with will generate a poem on the right side. For each new picture, a new poem will pop up.  Some words and lines of the poem will be static and made by us and then the tags will be used to fill out the rest of the poem. The pictures will change and so will the poem. The outcome will be a generative poem that relates to the displayed picture.

#### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?
For the flowchart it has been a bit difficult to be 100 percent clear in the representation of the process, before we've started actually programming the things we ideally want to be present on the site. We found that it was actually a good exercise for us, because it forced us to be very specific in each category of the programming. This also led us to be even more concordant in regards to starting up our final project.
Since none of us are originally programmers our thoughts tend to focus more on how we want to present the program aesthetically and conceptually, but this way of starting the process also made us think of how to do it in coding, which has been super helpful. 

#### What are the technical challenges facing the two ideas and how are you going to address these?
For both of our ideas we would like to use the EveryPixel API to tag some pictures that we choose. Our very first technical challenge has been getting access to this API, which is still something we’re working on. If we can’t get this to work we might have to consider using another similar one, or rethinking our entire idea.  <br>
For the first idea we’ll have some restrictions in generating our own tags. Since we have limited time we’ll never be able to generate enough tags for endless amounts of pictures. This means we’ll have to have a limited amount of pictures. <br>
For the second idea we are facing some technical challenges in terms of what source we should use - API or JSON. We were considering the API since it may seem easier to gather the pictures with the right tags that belong to the picture. In that way we would be sure that the tags are matching the giving picture that is shown on the canvas. Furthermore, we faced a few difficulties with the API’s. We think that it might be too complicated to create JSON files where the tags and the pictures are seperated. This would also take away the aspect of the machine learning - since it would not be generated from the actual picture, but randomly instead. <br>
Earlier on in the process we were looking into various API’s, for example from Twitter, but this would mean that we had to wait for a long period of time before we could gain access. Twitter also wanted to know the specific purpose of our project, what our backgrounds were, and what methods we were using to achieve our results. Based on the complications we chose not to work with the Twitter API. 

#### In which ways are the individual and the group flowcharts you produced useful? 
As mentioned above the group flowcharts have been very useful for creating a collective idea and understanding in everyone's mind about the conceptual and technical ideas for the runme. The flowcharts also makes it easier to begin the design process of the program because the basic functions have already been made. <br>

![Screenshot1](FlowchartMathilde.png)

The individual flowchart is useful for rethinking our original thought process. By working backwards in this manner, we were able to reflect on why we made certain decisions in our program. This has a lot of value for us going forward in the programming process, both individually, but also for us as a group. I made the choice to do my object orientation/ game miniX as a flowchart, because I didn't get it to work as planned and therefore I wanted to do the flowchart in order to illustrate how the game should work. This also gives me a great headstart if I ever go back over that project to rethink it.  




