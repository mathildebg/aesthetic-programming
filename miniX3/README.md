# READ ME for my MiniX3
----
## MiniX3 for Aesthetic Programming at Aarhus University


![Screenchot of my first program](miniX3screen.png)


### Link for my [MiniX3](https://mathildebg.gitlab.io/aesthetic-programming/miniX3/)
My RunMe is on the link above

### Link for my repository - [the code](https://gitlab.com/mathildebg/aesthetic-programming/-/blob/master/miniX3/sketch3.js)
My repository/the code for the program is on the link above

----

### README
#### Describe your program, both conceptually and technically
For this week I was superstressed out, and things were a bit hectic for me. So I decided to settle with a very simple design and simply investigate in the thrubber we made during class, and try to make a program from that. This means that the program in itself is fairly easy. I have made three flowers in different colorschemes that are pending on being fully drawn on a yellow screen. I wrote the text "..breathe" and "take a break.." on the site with an imported font. <br>
This is meant to put focus on exactly what has been my problem this week ironically enough. The thrubber is often of something annoying - that you have to wait, and often you don't recognize the site as anything but a waitingsite - time waisted. If you can use this time to breathe and calm down - take a minute to focus on what is important and not getting stressed out or frustruated. <br>
With that being said I didn't use a lot of focus on the program itself, but more on the concept and thought behind. That also means that I haven given much thought to the time-related aspects to be completely transparent. It's definitely the low floors for this week, though I am content with the look of my program. 


#### Think about a throbber that you have encounted in digital culture
Actually for the website I'm working on we've had some issues with the processing site of payments. It's a fairly simple thrubber, but everyone seems to know what it means. The problem however has been that it initiated, but then stopped without any change to the site. This has brought a lot of frustration and confusion to our customers. The problem seems to be that with the thrubber you get the feeling that something is actually processing, even though they're not technically linked. <br>
Generally I believe there are not a lot of positive feelings connected to a thrubber - it's never a positive experience to get exposed to a thrubber. We're so stressed and just wanting to focus on getting things done and overwith. Often times we even have a certain amount of time that we believe a thrubber should turn before we get impatient - this is also a very subjective amount of time. Personally I've encountered people with older computers that have a lot more patience than I do myself, because that's just how it is - our brains have just slowly been used to always getting new info. Back in the days I remember it taking 5 minutes to even get my computer started. <br>
Our whole world is so fast-paced and with this design I wanted to slow down and maybe look at the waiting time as something positive. An option for us to look at something beautiful (although my flowers aren't exactly beautiful :((, but the reference is ), and plants take time to grow. Patience is key. 




