# READ ME for my MiniX5
----
## MiniX5 for Aesthetic Programming at Aarhus University

![Screenchot of my first program](minix5.png)



### Link for my [MiniX5](https://mathildebg.gitlab.io/aesthetic-programming/minix5/)
My RunMe is on the link above

### Link for my repository - [the code](https://gitlab.com/mathildebg/aesthetic-programming/-/blob/master/minix5/sketch5.js)
My repository/ the code for the program is on the link above

----

### README

#### What are the rules in your generative program?
**Rule number 1** - for every ellipse draw a new one 40 pixels to the right, when it reaches the width of the screen continue 40 pixels below <br>
**Rule number 2** - the further down the x-axis the wider the ellipses and the further down the y-axis the longer the ellipses 

#### How does the program perform over time?
As the FrameCount goes up the image is looking more and more like a landscape. The difference in shape of the ellipses create a variation that gives dimension to the page. In the beginning it's very visible that the program is drawing individual ellipses, but towards the end it looks more like a combined image. <br>
The programme ends when the x < width and y < height. I considered adding some sort of automatic refresh that would start the programme over at this point, but I actually quite liked how it gives a finished look in the end. 

#### How do the rules produce emergent behavior?
The rules create both the variation in the size of the ellipses and the movement that creates the finished image. Those two things combined with the random function for color is what gives the programme its expression. 

#### What role do rules and processes have in your work?
I must admit I don't really like working with rules on beforehand as I think it sometimes restricts the outcome in the end. For this miniX I wanted something that was mainly just an artistic visible programme and I found it was a bit more difficult to get an abstract feel to it, when I had to make the rules first. Although within the rules I did successfully change the appearance of the programme a few times I found it restrictive to work this way. <br>
In the future I think I'll find it way more helpful to write down a bunch of ideas and be flexible with the final outcome in a way this miniX didn't allow me to. 

#### How does this miniX help you understand the idea of "auto-generator"?
I definitely feel as if I don't really any control of this programme even though I can point out exactly why it does each thing in on the site. This definitely was a way for me to reflect on how we can experience the difference between human art and programming art. <br>
This notion of is it human or is it the computer is still a difficult topic to handle, because in the end computers only exist because of humans. However I don't personally feel I have a direct connection to the outcome of the auto-generated outcomes especially because you as a user only look at it, and don't interact. I designed it, but the computer is processing and showing it, so maybe it is just a collaboration?





