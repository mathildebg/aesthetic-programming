let inconsolata;
function preload() {
  inconsolata = loadFont('Inconsolata-Black.otf');

  }
function setup(){
createCanvas(windowWidth,windowHeight);
frameRate(8)
  }

  function draw(){
background(250,233,122,60)
drawElements();
fill(169,220,51);
noStroke()
rect(windowWidth/2, windowHeight/2+12.5, 1.5,800);
fill(255,204,102);

ellipse(windowWidth/2,windowHeight/2, 25,25)
ellipse(windowWidth/5,windowHeight/4.5, 25,25)
ellipse(windowWidth-300,windowHeight/6, 25,25)

fill(169,220,51);
noStroke()
rect(windowWidth/5, windowHeight/4.5+12.5, 1.5,800);
fill(169,220,51);
noStroke()
rect(windowWidth-300, windowHeight/6+12.5, 1.5,800);

fill(169,220,51);
textFont(inconsolata)
textSize(20)
textAlign(CENTER);
text('.. take a break', 0.5*windowWidth+300, 300);
text('breathe..', 0.5*windowWidth-200, 200);
  }
function drawElements(){
  let num=25;
  push()
  translate(windowWidth/2,windowHeight/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir))
  noStroke();
  fill(195,163,243);
  ellipse(35,0,250,22);

  fill(235,162,240)
  ellipse(35,0,200,22)

  fill(217,186,235)
  ellipse(35,0,150,22)

  fill(235,186,223)
  ellipse(35,0,100,22)

  stroke(255,255,0);
  pop()

  push()
  translate(windowWidth/5,windowHeight/4.5);
  rotate(radians(cir))
  noStroke();
  fill(212,107,142)
  ellipse(35,0,200,22)
  fill(192,116,173)
  ellipse(35,0,150,22)
  fill(165,113,183)
  ellipse(35,0,100,22)
  stroke(255,255,0);
  pop()

  push()
  translate(windowWidth-300,windowHeight/6);
  rotate(radians(cir))
  noStroke();
  fill(103,183,181)
  ellipse(35,0,150,22)
  fill(135,228,224)
  ellipse(35,0,120,22)
  fill(33,172,167)
  ellipse(35,0,90,22)

  stroke(255,255,0);

  pop()

}
